class Strings {
  Strings._();
  
  static const String url = 'https://cascada-qr-code.brij.tech/';
  static const String m3u8Url = 'https://d3md4bsuwalvsr.cloudfront.net/8b8e23a0-0dc2-456c-9bfb-28c64cd1c970/AppleHLS1/75a1d573-1937-47b7-8387-9a2654edb72c.m3u8';
}
