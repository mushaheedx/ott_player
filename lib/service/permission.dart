import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:permission_handler/permission_handler.dart';

class PermissionsManager {
  static void check(BuildContext context) async {
    var status = await Permission.camera.status;
    print('Camera permission status: $status');
    if (status.isDenied) {
      // We didn't ask for permission yet or the permission has been denied before but not permanently.
      final response = await Permission.camera.request();
      if (response.isGranted) {
        return;
      }
    }

    final response = await Permission.camera.request();

    // You can can also directly ask the permission about its status.
    if (!response.isGranted) {
      // The OS restricts access, for example because of parental controls.
      final snackclosed = _showMessage(context, 'App needs camera permissions');
      await snackclosed.closed;
      await openAppSettings();
    }
  }

  static ScaffoldFeatureController<SnackBar, SnackBarClosedReason> _showMessage(
      BuildContext context, String text) {
    return ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        backgroundColor: Colors.red,
        content: Text(
          text,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
