import 'package:flutter/services.dart';
import 'package:ott_player/screen/player_screen.dart';
import 'package:flutter/material.dart';

import 'commons/strings.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Video Player',
      theme: ThemeData(
        primarySwatch: Colors.grey,
        brightness: Brightness.dark,
      ),
      home: MyHomePage(),
      // shortcuts: <LogicalKeySet, Intent>{
      //  // ...WidgetsApp.defaultShortcuts,
      //   LogicalKeySet(LogicalKeyboardKey.select): ActivateIntent(),
      // },
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<FormFieldState<String>> _videoUrlKey =
      GlobalKey<FormFieldState<String>>();
  final FocusNode openPlayerFocusNode = FocusNode();

  @override
  void initState() {
    // WidgetsBinding.instance?.addPostFrameCallback((_) {
    //   PermissionsManager.check(context);
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Video player'),
      ),
      body: Center(
        child: Form(
          key: _formKey,
          autovalidateMode: AutovalidateMode.always,
          child: ListView(
            padding: const EdgeInsets.symmetric(
              horizontal: 22,
              vertical: 16,
            ),
            shrinkWrap: true,
            children: [
              TextFormField(
                key: _videoUrlKey,
                initialValue: Strings.m3u8Url,
                decoration: InputDecoration(
                  labelText: 'Video URL',
                ),
                validator: (value) {
                  if (value?.isEmpty ?? true) {
                    return 'Video URL must not be empty';
                  }
                },
                onEditingComplete: () {
                  openPlayerFocusNode.requestFocus();
                },
                onFieldSubmitted: (_) {
                  openPlayerFocusNode.requestFocus();
                },
                onSaved: (_) {
                  openPlayerFocusNode.requestFocus();
                },
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          final _isValid = _formKey.currentState?.validate() ?? false;
          if (_isValid) {
            final _videoUrl = _videoUrlKey.currentState!.value!;

            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) {
                  return VideoPlayerScreen(
                    videoUrl: _videoUrl,
                  );
                },
              ),
            );
          }
        },
        focusNode: openPlayerFocusNode,
        focusColor: Colors.blue,
        label: Text('Open'),
      ),
    );
  }
}
